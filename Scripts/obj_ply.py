#Author: Alexander Griessel
#Created On: 14/11/2019

import pywavefront
import numpy as np
from plyfile import PlyData, PlyElement
import os as file_manager
import json
from defs.mesh import Mesh
from pywavefront import visualization
import math

def start():
    """Entry point"""
    files = file_manager.listdir("./system/model/wavefronts/")
    print(f"Found the following Files: {files}")
    for file in files:
        name = file[:-4]
        print(f"Currently working on: {file}")
        if ".obj" in file:
            file = "./system/model/wavefronts/" + file
            print(file)
            try:
                vertices, faces = read_obj(file)
                build_output_grasps(vertices, name)
                ply = obj_to_ply(name, vertices, faces)
            except Exception as error:
                print("Error: ")
                print(error)


def read_obj(file):
    """read the obj file"""
    try:
        object = pywavefront.Wavefront(file, create_materials=True, collect_faces=True)
        object.parse()
        print(f"Opened {file}")
        vertices = object.vertices
        faces = object.mesh_list[0].faces
        return vertices, faces
    except Exception as error:
        print("Error: ")
        print(error)

def obj_to_ply(name, vertices, faces):
    """Converts obj into PLY"""
    x, y, z = get_vert_positions(vertices)
    affordability = get_grasp_strength(name, vertices)
    red = affordability
    green = affordability
    blue = affordability
    el, el2 = get_ply_data(faces, x, y, z, red, green, blue)
    plydata = PlyData([el, el2])
    PlyData(plydata, text=True).write("./wavefronts/" + name + ".ply")
    return plydata


def get_vert_positions(vertices):
    """Get the correct vertex positions"""
    x = []
    y = []
    z = []
    for vert in vertices:
        x.append(vert[0])
        y.append(vert[1])
        z.append(vert[2])
    return x, y, z


def get_grasp_strength(name, vertices):
    """Calculates the grasp strnegth of vertices"""
    grasps = []
    with open("./system/model/data/files/" + name + "_unity_lbl.json", "r") as grasps:
        data_json = json.load(grasps)
        grasps = data_json.get("Grasped")
    num = 0
    for grasp in grasps:
        if grasp > num:
            num = grasp
    affordability = []
    for i in range(len(vertices)):
        aff = grasps[i]
        aff = (aff / num) * 255
        affordability.append(aff)
    return affordability


def get_ply_data(new_faces, x, y, z, red, green, blue):
    """ generate the ply data elements"""
    verts = []
    faces = []
    for i in range(len(x)):
        verts.append((x[i] ,y[i] ,z[i] ,red[i] ,green[i] ,blue[i]))
    for face in new_faces:
        faces.append(([face[0], face[1], face[2]], 0))
    vertex = np.array(verts, dtype=[("x", "f8"), ("y", "f8"), ("z", "f8"), ("red", "u1"), ("green", "u1"), ("blue", "u1")])
    face = np.array(faces, dtype=[("vertex_indices", "int", (3,)),('s', 'u1')])
    el = PlyElement.describe(vertex, "vertex")
    el2 = PlyElement.describe(face, "face")
    return el, el2


def build_output_grasps(obj_verts, name):
    """Preserves grasping data through transition"""
    positions = []
    for vert in obj_verts:
        positions.append([vert[0], vert[1], vert[2]])
    np.save("./system/model/data/files/" + name + "_obj_index.npy", np.array(positions))


start()