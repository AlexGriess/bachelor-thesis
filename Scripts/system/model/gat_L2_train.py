from __future__ import absolute_import, division, print_function, unicode_literals
import matplotlib.pyplot as plt
import numpy as np
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from res.utils import load_probability_data, load_meshes, load_probability_pred_data
from keras_gat import GraphAttention
from keras.layers import LeakyReLU
from os import getcwd
import os.path as path
import os
import json
import random
import datetime

# Install TensorFlow
import tensorflow as tf

"""
The Objective of the GAT layers is to classify a node as being a likely affordance or not.
After the Network has evaluated a mesh, a list of affordances remains.
This list of affordances can then be post processed to find ideal grasping points.
Validation data is generated through human interaction with various objects.
Many interactions will need to be captured in order to identify a sensible grasping strategy.
One object may contain many affordances.
The final output is a degree of likely hood. Ideally, these likelihoods will center around a singular point.
"""
file_weights = "/logs/best_model_l2.h5"
meshes = load_meshes()

#CreateModel
X, A, Y_train, idx_train, idx_val, idx_test = load_probability_data(meshes[0])
print(X[0])
F = X.shape[1]              # Total number of Features on each node
print(F)
print(Y_train.shape)
F_1 = 8                    # Output number of Features of Layer 1
F_2 = 8                    # Output number of Features of Layer 2
F_3 = 1

n_heads_1 = 10               # Number of attention heads in the first layer
n_heads_2 = 10              # Number of attention heads in the second layer
  

# Set network parameters
dropout_rate = 0.2          # Dropout rate between/inside GAT layers
learning_rate = 8e-5        # Learning rate
epochs = 4000                # Number of training epochs
es_patience = 100           # Early stopping patience
l2_reg = 3e-3/2


# Get data parameters
N = X.shape[0]              # Number of Nodes in the mesh


# Model definition
X_in = Input(shape=(F,))
N = X.shape[0]              # Number of Nodes in the mesh
print(N)
A_in = Input(shape=(N,))


# Define GAT layers
dropout1 = Dropout(dropout_rate)(X_in)
graph_attention_1 = GraphAttention(F_1,
                                    attn_heads=n_heads_1,
                                    attn_heads_reduction='concat',
                                    dropout_rate=dropout_rate,
                                    activation=LeakyReLU(alpha=0.3),
                                    kernel_initializer='he_uniform',
                                    bias_initializer='zeros',
                                    attn_kernel_initializer='he_uniform',
                                    kernel_regularizer=l2(l2_reg),
                                    attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])
dropout2 = Dropout(dropout_rate)(graph_attention_1)
graph_attention_2 = GraphAttention(F_3,
                                    attn_heads=1,
                                    attn_heads_reduction='average',
                                    kernel_initializer='random_uniform',
                                    bias_initializer='zeros',
                                    attn_kernel_initializer='random_uniform',
                                    dropout_rate=dropout_rate,
                                    activation='linear',
                                    kernel_regularizer=l2(l2_reg),
                                    attn_kernel_regularizer=l2(l2_reg))([dropout2, A_in])


# Build model
model = Model(inputs=[X_in, A_in], outputs=graph_attention_2)
optimizer = Adam(lr=learning_rate)
model.compile(optimizer=optimizer,
                loss='mean_absolute_error',
                weighted_metrics=['mae','acc'])
            
model.summary()
    

#Enable callbacks
log_dir="./logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
es_callback = EarlyStopping(monitor='val_weighted_acc', patience=es_patience)           # Configure Early Stopping
tb_callback = TensorBoard(histogram_freq=1,log_dir=log_dir,batch_size=N)                # Visualize data
mc_callback = ModelCheckpoint(getcwd() +'/logs/best_model_l2.h5',                       # Save best model
                            monitor='val_mae',
                            save_best_only=True,
                            save_weights_only=True)


#Train model here
Y_val = Y_train
validation_data = ([X, A], Y_train, idx_val)
history = model.fit([X, A],
        Y_train,
        sample_weight=idx_train,
        epochs=epochs,
        batch_size=N,
        shuffle=False,  # Shuffling data means shuffling the whole graph
        validation_data=validation_data,
        callbacks=[es_callback, tb_callback, mc_callback])


# Load best model
model.load_weights(getcwd() + '/logs/best_model_l2.h5')


# Evaluate model
eval_results = model.evaluate([X, A],
                        Y_train,
                        sample_weight=idx_test,
                        batch_size=N,
                        verbose=0)
print('Done.\n'
'Test loss: {}\n'
'Test accuracy: {}'.format(*eval_results))
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
plt.plot(history.history['loss']) 
plt.plot(history.history['val_loss']) 
plt.yscale("log")
plt.title('Model loss') 
plt.ylabel('Loss') 
plt.xlabel('Epoch') 
plt.legend(['Train', 'Test'], loc='upper left') 
plt.show()
plt.plot(history.history['mae']) 
plt.plot(history.history['val_mae']) 
plt.yscale("log")
plt.title('Model mae') 
plt.ylabel('Mae') 
plt.xlabel('Epoch') 
plt.legend(['Train', 'Test'], loc='upper left') 
plt.show()