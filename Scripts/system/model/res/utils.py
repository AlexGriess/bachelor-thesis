from keras.utils import to_categorical
from plyfile import PlyData, PlyElement
from random import random
from random import randint
import numpy as np
import json
import os
import vg


dir_meshes = "./data/files/"

def load_meshes():
    """Get the list of all mesh training data"""
    with open("./data/data.json") as meshes:
        data = json.load(meshes)
        mesh_names = list(data.keys())
    return mesh_names


#Deprecated
def load_data(mesh_name):
    """ Load all the data for a specific mesh"""
    return None

def load_probability_pred_data(mesh_name, meshes):
    """Loads all the masks, inputs, and classes"""
    X_train = load_probability_inputs(meshes[0])
    X = z_score_standard(load_probability_inputs(mesh_name), X_train)
    A = load_adjacency(mesh_name)
    return X, A


def load_probability_data(mesh_name):
    """Loads all the masks, inputs, and classes"""
    X = z_score_features(load_probability_inputs(mesh_name))
    print(X.shape)
    A = load_adjacency(mesh_name)
    Y = load_probability_classes(mesh_name)
    idx_train, idx_val, idx_test = gen_masks(Y)
    return X, A, Y, idx_train, idx_val, idx_test


def load_probability_inputs(mesh_name):
    """ Load all the inputs for the affordance probability stage"""
    features = np.load(dir_meshes + mesh_name +"_feats.npy")
    return features


def load_probability_classes(mesh_name):
    """Loads the vertex affordances"""
    Y = np.load(dir_meshes + mesh_name + "_aff.npy")
    print(Y.shape[0])
    Y.reshape(Y.shape[0], 1)
    print(Y)
    return Y

def load_grasping_pred_data(mesh_name, meshes, vertex=None):
    """Load data for one mesh"""
    basic_meshes = ["phone", "soda", "cube_lp", "banana Variant"]
    X_train, Y, classes, idx_train, idx_val, idx_test = load_grasping_data(basic_meshes)
    features = np.load(dir_meshes + mesh_name + "_aff_feats.npy")
    probabilities = np.load(dir_meshes + mesh_name + "_L2.npy")
    if not vertex:
        vertex = randint(0, len(probabilities))
    probabilities = np.reshape(probabilities,len(probabilities),)
    tup = zip(probabilities, range(len(probabilities)))
    sorted_probabilities = sorted(tup, key=lambda v: v[0], reverse=True)
    best = sorted_probabilities[:1]
    best = [i for i in range(len(probabilities))]
    X_best = []
    #best = [vertex]
    for v in best:
        X = []
        for i in range(len(features)):
            x_1 = features[v]
            x_2 = features[i]
            pos_1 = np.array([x_1[0], x_1[1], x_1[2]])
            pos_2 = np.array([x_2[0], x_2[1], x_2[2]])
            n_1 = np.array([x_1[3], x_1[4], x_1[5]])
            n_2 = np.array([x_2[3], x_2[4], x_2[5]])
            dist = vg.euclidean_distance(pos_1, pos_2)
            possible = 1
            if dist > 0.09 or dist < 0.00:
                possible = 0
            angle = vg.angle(n_1, n_2)
            span_vec = np.subtract(pos_1, pos_2)
            d_1 = 0
            d_2 = 0
            if vg.magnitude(span_vec) < 0.01:
                span_vec = np.array([0,0,0])
            else:
                d_1 = vg.angle(n_1, span_vec)/180
                d_2 = vg.angle(n_2, span_vec)/180
            X.append([dist, angle, x_1[6], x_2[6], d_1, probabilities[v]*possible, probabilities[i]*possible])
        X = z_score_standard(np.array(X), X_train)
        X_best.append(X)
    return X_best, best

def load_grasping_data(meshes):
    """Loads all the masks, inputs, and classes"""
    meshes = ["phone", "soda", "cube_lp", "banana Variant"]
    Y = load_grasping_classes(meshes[0])
    X, classes = load_grasping_input(meshes[0], Y)
    print(meshes[0])
    print(X.shape)
    print(X.shape)
    print("____________________________________")
    for i in range(1, len(meshes)):
        y = load_grasping_classes(meshes[i])
        x, c = load_grasping_input(meshes[i], y)
        Y = np.concatenate((Y, y))
        X = np.concatenate((X, x))
        classes = np.concatenate((classes, c))
        print(meshes[i])
        print(X.shape)
        print(x.shape)
        print(Y.shape)
        print(y.shape)
        print(classes.shape)
        print(c.shape)
        print("____________________________________")
    idx_train, idx_val, idx_test = gen_masks(classes)
    X = z_score_features(X)
    return X, Y, classes, idx_train, idx_val, idx_test


def load_grasping_input(mesh_name, Y):
    """ Loads the features for each vertex"""
    features = np.load(dir_meshes + mesh_name + "_aff_feats.npy")
    probabilities = np.load(dir_meshes + mesh_name + "_aff.npy")
    probabilities = np.reshape(probabilities,len(probabilities),)
    X = []
    classes = []
    for y in Y:
        y_1 = (int)(y[0])
        y_2 = (int)(y[1])
        x_1 = features[y_1]
        x_2 = features[y_2]
        pos_1 = np.array([x_1[0], x_1[1], x_1[2]])
        pos_2 = np.array([x_2[0], x_2[1], x_2[2]])
        n_1 = np.array([x_1[3], x_1[4], x_1[5]])
        n_2 = np.array([x_2[3], x_2[4], x_2[5]])
        dist = vg.euclidean_distance(pos_1, pos_2)
        angle = vg.angle(n_1, n_2)
        span_vec = np.subtract(pos_1, pos_2)
        d_1 = 0
        d_2 = 0
        if vg.magnitude(span_vec) < 0.01:
            span_vec = np.array([0,0,0])
        else:
            look = vg.perpendicular(n_1, span_vec)
            d_1 = vg.angle(n_1, span_vec)/180
            d_2 = vg.angle(n_2, span_vec)/180
        X.append([dist, angle, x_1[6], x_2[6], d_1, probabilities[y_1], probabilities[y_2]])
        clss = y[2]
        classes.append(clss)
    return np.array(X), np.array(classes)


def calc_displ(n_1, n_2, pos_1, pos_2):
    """Calculate displacement"""
    if pos_1 is not pos_2:
        span_vec = np.subtract(pos_1, pos_2)
        d_1 = vg.angle(n_1, span_vec)/180
        d_2 = vg.angle(n_2, span_vec)/180
        return d_1, d_2
    else:
        return 0,0



def load_grasping_classes(mesh_name):
    """Collects the grasping pairs"""
    Y = np.load(dir_meshes + mesh_name + "_grasps.npy")
    return Y

def load_adjacency(mesh_name):
    """ Load adjacency matrix for mesh"""
    A = np.load(dir_meshes + mesh_name + "_mat.npy")
    return A


def gen_masks(labels):
    """ Generate Training Masks"""
    idx_train = []
    idx_val = []
    idx_test = []
    for i in range(len(labels)):
        rand = random()
        if rand < 0.66:
            idx_train.append(True)
            idx_test.append(False)
            idx_val.append(False)
        elif rand > 0.83:
            idx_train.append(False)
            idx_test.append(True)
            idx_val.append(False)
        else:
            idx_train.append(False)
            idx_test.append(False)
            idx_val.append(True)
    idx_train = np.reshape(np.array(idx_train), len(labels))
    idx_test = np.reshape(np.array(idx_test), len(labels))
    idx_val = np.reshape(np.array(idx_val), len(labels))
    return idx_train, idx_val, idx_test


def z_score_standard(features, training):
    """Z_score features"""
    F = features.T
    T = training.T
    features_Z = np.array([get_z_standard(F[i], T[i]) for i in range(len(F))]).T
    return features_Z

def z_score_features(features):
    """Z_scores all features"""
    features_Z = np.array([get_z_score(feats) for feats in features.T]).T
    return features_Z


def get_z_score(values):
    """returns Z score of all values"""
    s = np.std(values)
    m = np.mean(values)
    z_scores = [(x - m)/s for x in values]
    return z_scores


def get_z_standard(f, t):
    """does"""
    s = np.std(t)
    m = np.mean(t)
    z_scores = [(x - m)/s for x in f]
    return z_scores