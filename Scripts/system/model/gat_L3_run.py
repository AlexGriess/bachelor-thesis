import numpy as np
import vg
import math
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.models import Sequential
from keras.layers import Dense
from res.utils import load_meshes, load_grasping_pred_data
from os import getcwd
import os.path as path
import datetime
from keras.layers import LeakyReLU
from plyfile import PlyData, PlyElement
from random import random

meshes = load_meshes()
X_best, best = load_grasping_pred_data(meshes[0], meshes)
file_weights = "./logs/best_model_l3.h5"

# Define parameters
input_dim = X_best[0].shape[1]
nodes_l_1 = 16
nodes_l_2 = 16
nodes_l_3 = 16
nodes_out = 1
batch_size = 8
learning_rate = 6e-3        # Learning rate
optimizer = Adam(lr=learning_rate)

# Define model
model = Sequential()
model.add(Dense(nodes_l_1, input_dim=input_dim, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_l_2, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_l_3, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_out, activation="linear", kernel_initializer="random_uniform"))
model.compile(optimizer=optimizer, loss='mean_squared_error', metrics=['mse'])
model.summary()
model.load_weights(getcwd() + file_weights)
for mesh in meshes:
    X_best, best = load_grasping_pred_data(mesh, meshes)
    print("_______________________________")
    print(f"Prediction on mesh: {mesh}")
    indexes = []
    highest = []
    length = 0
    print(f"Vertex Length: {len(X_best)}")
    for X, vertex in zip(X_best, best):
        predictions = model.predict(X,
                                    batch_size=batch_size)
        predictions = np.reshape(predictions,len(predictions),)
        tup = zip(predictions, range(len(predictions)))
        sorted_predictions = sorted(tup, key=lambda v: v[0], reverse=True)
        best_pred = sorted_predictions[:1]
        indexes.append(best_pred[0][1])
        highest.append(best_pred[0][0])
        #indexes.append(best_pred[0][1])
        length = len(predictions)
    print(length)
    tup = zip(highest, range(len(highest)))
    sorted_highest = sorted(tup, key=lambda v: v[0], reverse=True)
    best_pred = sorted_highest[:1]
    print(best_pred)
    print(f"Best pred: {best_pred[0][1], indexes[best_pred[0][1]]}, {best_pred[0][0]}")
    red = [0 for i in range(length)]
    blue = [0 for i in range(length)]
    green = [0 for i in range(length)]
    r_1 = random() * 255
    r_2 = random() * 255
    r_3 = random() * 255
    red[best_pred[0][1]] = r_1
    red[indexes[best_pred[0][1]]] = r_1
    green[best_pred[0][1]] = r_2
    green[indexes[best_pred[0][1]]] = r_2
    blue[best_pred[0][1]] = r_3
    blue[indexes[best_pred[0][1]]] = r_3


    data = PlyData.read("./plys/originals/" + mesh + ".ply")
    data['vertex']['red'] = red
    data['vertex']['green'] = green
    data['vertex']['blue'] = blue
    vertex = data['vertex']
    PlyData(data, text=True).write("./plys/" + mesh + "_L3.ply")

"""
# Run predictions
for mesh in meshes:
    print("_______________________________")
    print(f"Prediction on mesh: {mesh}")
    X, vertex = load_grasping_pred_data(mesh)
    predictions = model.predict(X,
                                batch_size=batch_size)
    print(predictions[0])
    predictions = np.reshape(predictions,len(predictions),)
    tup = zip(predictions, range(len(predictions)))
    sorted_predictions = sorted(tup, key=lambda v: v[0], reverse=True)
    print(sorted_predictions[:4])

    data = PlyData.read("./plys/originals/" + mesh + ".ply")
    data['vertex']['red'] = scale
    data['vertex']['green'] = scale
    data['vertex']['blue'] = scale
    vertex = data['vertex']
    PlyData(data, text=True).write("./plys/" + mesh + "_L2.ply")
"""




