from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from res.utils import load_data, load_meshes
from keras.layers import LeakyReLU

from keras_gat import GraphAttention
import numpy as np
from os import getcwd
import os.path as path
import os
import json
import random
import datetime

# Install TensorFlow
import tensorflow as tf

"""
The Objective of the GAT layers is to classify a node as being a likely affordance or not.
After the Network has evaluated a mesh, a list of affordances remains.
This list of affordances can then be post processed to find ideal grasping points.
Validation data is generated through human interaction with various objects.
Many interactions will need to be captured in order to identify a sensible grasping strategy.
One object may contain many affordances.
The final output is a degree of likely hood. Ideally, these likelihoods will center around a singular point.
"""
file_weights = "/logs/best_model_l1.h5"
meshes = load_meshes()

#CreateModel
X, A, Y_train, idx_train, idx_val, idx_test = load_data(meshes[0])
F = X.shape[1]              # Total number of Features on each node
F_1 = 16                    # Output number of Features of Layer 1
F_2 = 12                    # Output number of Features of Layer 2
F_3 = 5

n_heads_1 = 10              # Number of attention heads in the first layer
n_heads_2 = 15              # Number of attention heads in the second layer  

# Set network parameters
dropout_rate = 0.5          # Dropout rate between/inside GAT layers
learning_rate = 1e-5       # Learning rate
epochs = 5000               # Number of training epochs
es_patience = 150           # Early stopping patience
l2_reg = 1e-3/2              #5e-4/2


for i in range(1):
    for mesh in meshes:
        # Define GAT layers
        print("Current training data: " + mesh)
        X, A, Y_train, idx_train, idx_val, idx_test = load_data(mesh)
        # Get data parameters
        N = X.shape[0]              # Number of Nodes in the mesh
        # Model definition
        X_in = Input(shape=(F,))
        N = X.shape[0]              # Number of Nodes in the mesh
        A_in = Input(shape=(N,))
        dropout1 = Dropout(dropout_rate)(X_in)
        """
        graph_attention_2 = GraphAttention(F_2,
                                            attn_heads=n_heads_2,
                                            attn_heads_reduction='concat',
                                            dropout_rate=dropout_rate,
                                            activation=LeakyReLU(alpha=0.5),
                                            kernel_initializer='he_uniform',
                                            bias_initializer='zeros',
                                            attn_kernel_initializer='he_uniform',
                                            kernel_regularizer=l2(l2_reg),
                                            attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])
        dropout4 = Dropout(dropout_rate)(graph_attention_2)"""
        graph_attention_4 = GraphAttention(F_3,
                                            attn_heads=4,
                                            attn_heads_reduction='average',
                                            kernel_initializer='glorot_uniform',
                                            bias_initializer='zeros',
                                            attn_kernel_initializer='glorot_uniform',
                                            dropout_rate=dropout_rate,
                                            activation='softmax',
                                            kernel_regularizer=l2(l2_reg),
                                            attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])


        # Build model
        model = Model(inputs=[X_in, A_in], outputs=graph_attention_4)
        optimizer = Adam(lr=learning_rate)
        model.compile(optimizer=optimizer,
                        loss='categorical_crossentropy',
                        weighted_metrics=['acc'])
        
        if path.exists(getcwd() + file_weights):
            model.load_weights(getcwd() + file_weights)
            os.remove(getcwd() + file_weights)
        else:
            print("No weights")
            
        model.summary()
    

        #Enable callbacks
        log_dir="./logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        es_callback = EarlyStopping(monitor='val_weighted_acc', patience=es_patience)       # Configure Early Stopping
        tb_callback = TensorBoard(histogram_freq=1,log_dir=log_dir,batch_size=N)                                             # Visualize data
        mc_callback = ModelCheckpoint(getcwd() +'/logs/best_model_1.h5',                                 # Save best model
                                    monitor='val_acc',
                                    save_best_only=True,
                                    save_weights_only=True)

        #Train model here
        Y_val = Y_train
        validation_data = ([X, A], Y_train, idx_val)
        history = model.fit([X, A],
                Y_train,
                sample_weight=idx_train,
                epochs=epochs,
                batch_size=N,
                shuffle=False,  # Shuffling data means shuffling the whole graph
                validation_data=validation_data,
                callbacks=[es_callback, tb_callback, mc_callback]

                )
        # Load best model
        model.load_weights(getcwd() + '/logs/best_model_1.h5')


        # Evaluate model
        eval_results = model.evaluate([X, A],
                                Y_train,
                                sample_weight=idx_test,
                                batch_size=N,
                                verbose=0)
        print(f'Completed cycle{i}')
        print('Done.\n'
        'Test loss: {}\n'
        'Test accuracy: {}'.format(*eval_results))
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()
        plt.plot(history.history['loss']) 
        plt.plot(history.history['val_loss']) 
        plt.title('Model loss') 
        plt.ylabel('Loss') 
        plt.xlabel('Epoch') 
        plt.legend(['Train', 'Test'], loc='upper left') 
        plt.show()

    random.shuffle(meshes)