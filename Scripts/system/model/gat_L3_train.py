import numpy as np
import vg
import math
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.models import Sequential
from keras.layers import Dense
from res.utils import load_meshes, load_grasping_data
from os import getcwd
import os.path as path
import datetime
from keras.layers import LeakyReLU
from plyfile import PlyData, PlyElement

meshes = load_meshes()
X, pairs, Y, idx_train, idx_val, idx_test = load_grasping_data(meshes)


# Define parameters
input_dim = 7
nodes_l_1 = 16
nodes_l_2 = 16
nodes_l_3 = 16
nodes_out = 1
epochs = 10
batch_size = 8
es_patience = 10
learning_rate = 0.002

# Define model
optimizer = Adam(lr=learning_rate)
model = Sequential()
model.add(Dense(nodes_l_1, input_dim=input_dim, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_l_2, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_l_3, activation=LeakyReLU(alpha=0.3), kernel_initializer="he_uniform"))
model.add(Dense(nodes_out, activation="linear", kernel_initializer="random_uniform"))
model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['mse'])
model.summary()

# Callbacks
log_dir="./logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
es_callback = EarlyStopping(monitor='mse', patience=es_patience)                   # Configure Early Stopping
tb_callback = TensorBoard(histogram_freq=1,log_dir=log_dir,batch_size=batch_size)                        # Visualize data
mc_callback = ModelCheckpoint(getcwd() +'/logs/best_model_l3.h5',                                  # Save best model
                            monitor='val_mse',
                            save_best_only=True,
                            save_weights_only=True)

# Train model
validation_data = (X, Y, idx_val)
history = model.fit(X,
                    Y, 
                    sample_weight=idx_train, 
                    validation_data=validation_data, 
                    epochs=epochs, 
                    batch_size=batch_size, 
                    callbacks=[mc_callback])


predictions = model.predict(X,
                                batch_size=batch_size)

for i in range(len(predictions)):
    print(Y[i])
    print(predictions[i])

plt.plot(history.history['mse'])
plt.plot(history.history['val_mse'])
plt.title('Model mse')
plt.ylabel('mse')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
plt.plot(history.history['loss']) 
plt.plot(history.history['val_loss']) 
plt.yscale("log")
plt.title('Model loss') 
plt.ylabel('Loss') 
plt.xlabel('Epoch') 
plt.legend(['Train', 'Test'], loc='upper left') 
plt.show()


