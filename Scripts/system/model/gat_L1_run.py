from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from res.utils import load_data, load_meshes
from plyfile import PlyData, PlyElement
from keras.layers import LeakyReLU
from keras_gat import GraphAttention
import numpy as np
from os import getcwd
import os.path as path
import os
import json
import random
import datetime

# Install TensorFlow
import tensorflow as tf

def serialize_output(predictions, mesh_name):
    """create output file"""
    output = np.array(predictions)
    np.save("./data/files/" + mesh_name + "_L1.npy", output)
    
def visualize_output(predictions):
    """serialize the data"""
    red = []
    green = []
    blue = []
    for predict in predictions:
        g = []
        b = []
        r = (int)(predict[1] * 255)
        if predict[2] > predict[3]:
            g = (int)(127.5 + predict[2]*127.5)
        else:
            g = (int)(predict[3]*127)
        if predict[2] > predict[4]:
            b = (int)(127.5 + predict[2]*127.5)
        else:
            b = (int)(predict[4]*127.5)
        red.append(r)
        green.append(g)
        blue.append(b)
    data = PlyData.read("./plys/" + mesh + ".ply")
    data['vertex']['red'] = red
    data['vertex']['green'] = green
    data['vertex']['blue'] = blue
    vertex = data['vertex']
    PlyData(data, text=True).write("./plys/" + mesh + ".ply")


file_weights = "/logs/best_model_1.h5"
meshes = load_meshes()
X, A, Y_train, idx_train, idx_val, idx_test = load_data(meshes[0])
F = X.shape[1]              # Total number of Features on each node
F_1 = 16                    # Output number of Features of Layer 1
F_2 = 12                    # Output number of Features of Layer 2
F_3 = 5
F_4 = 6
F_5 = 6      # Output number of Features of Layer 3
n_heads_1 = 10              # Number of attention heads in the first layer
n_heads_2 = 15              # Number of attention heads in the second layer
n_heads_3 = 8               # Number of attention heads in the third layer
n_heads_4 = 6               # Number of attention heads in the third layer
n_heads_5 = 6               # Number of attention heads in the third layer
  

# Set network parameters
dropout_rate = 0.2          # Dropout rate between/inside GAT layers
learning_rate = 6e-3        # Learning rate
epochs = 5000              # Number of training epochs
es_patience = 100           # Early stopping patience
l2_reg = 5e-4/2


# Define GAT layers
for mesh in meshes:
    print("Current training data: " + mesh)
    X, A, Y_train, idx_train, idx_val, idx_test = load_data(mesh)
    # Get data parameters
    N = X.shape[0]              # Number of Nodes in the mesh
    # Model definition
    X_in = Input(shape=(F,))
    N = X.shape[0]              # Number of Nodes in the mesh
    A_in = Input(shape=(N,))
    dropout1 = Dropout(dropout_rate)(X_in)
    """
    graph_attention_2 = GraphAttention(F_2,
                                        attn_heads=n_heads_2,
                                        attn_heads_reduction='concat',
                                        dropout_rate=dropout_rate,
                                        activation=LeakyReLU(alpha=1),
                                        kernel_initializer='he_uniform',
                                        bias_initializer='zeros',
                                        attn_kernel_initializer='he_uniform',
                                        kernel_regularizer=l2(l2_reg),
                                        attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])
    dropout4 = Dropout(dropout_rate)(graph_attention_2)"""
    graph_attention_4 = GraphAttention(F_3,
                                        attn_heads=4,
                                        attn_heads_reduction='average',
                                        kernel_initializer='glorot_uniform',
                                        bias_initializer='zeros',
                                        attn_kernel_initializer='glorot_uniform',
                                        dropout_rate=dropout_rate,
                                        activation='softmax',
                                        kernel_regularizer=l2(l2_reg),
                                        attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])


    # Build model
    model = Model(inputs=[X_in, A_in], outputs=graph_attention_4)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer,
                    loss='categorical_crossentropy',
                    weighted_metrics=['acc'])
        
    model.load_weights(getcwd() + file_weights)
    # Load best model
    model.load_weights(getcwd() + '/logs/best_model_1.h5')
    # Evaluate model
    """ eval_results = model.evaluate([X, A],
                            Y_train,
                            sample_weight=idx_test,
                            batch_size=N,
                            verbose=0)"""
    predictions = model.predict([X, A],
                                batch_size=N)
    """    print('Done.\n'
    'Test loss: {}\n'
    'Test accuracy: {}'.format(*eval_results))"""
    visualize_output(predictions)


