from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
from keras.callbacks import EarlyStopping, TensorBoard, ModelCheckpoint
from keras.layers import Input, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from res.utils import load_probability_data, load_meshes, load_probability_pred_data
from plyfile import PlyData, PlyElement
from keras.layers import LeakyReLU
from keras_gat import GraphAttention
import numpy as np
from os import getcwd
import os.path as path
import os
import json
import random
import datetime

# Install TensorFlow
import tensorflow as tf

def serialize_output(predictions, mesh_name):
    """create output file"""
    output = np.array(predictions)
    np.save("./data/files/" + mesh_name + "_L2.npy", output)
    
def visualize_output(predictions):
    """serialize the data"""
    high = 0
    for p in predictions:
        if p > high:
            high = p
    scale = [255 * (p/high) for p in predictions]
    data = PlyData.read("./plys/originals/" + mesh + ".ply")
    data['vertex']['red'] = scale
    data['vertex']['green'] = scale
    data['vertex']['blue'] = scale
    vertex = data['vertex']
    PlyData(data, text=True).write("./plys/" + mesh + "_L2.ply")


file_weights = "/logs/best_model_l2.h5"
meshes = load_meshes()
X, A, Y_train, idx_train, idx_val, idx_test = load_probability_data(meshes[0])
F = X.shape[1]              # Total number of Features on each node
F_1 = 8                    # Output number of Features of Layer 1
F_2 = 8                    # Output number of Features of Layer 2
F_3 = 1

n_heads_1 = 10              # Number of attention heads in the first layer
n_heads_2 = 10              # Number of attention heads in the second layer

  

# Set network parameters
dropout_rate = 0.2          # Dropout rate between/inside GAT layers
learning_rate = 6e-3        # Learning rate
epochs = 3000               # Number of training epochs
es_patience = 100           # Early stopping patience
l2_reg = 5e-4/2


# Define GAT layers
for mesh in meshes:
    print("Current training data: " + mesh)
    X, A= load_probability_pred_data(mesh, meshes)
    # Get data parameters
    N = X.shape[0]              # Number of Nodes in the mesh
    # Model definition
    X_in = Input(shape=(F,))
    N = X.shape[0]              # Number of Nodes in the mesh
    A_in = Input(shape=(N,))
    dropout1 = Dropout(dropout_rate)(X_in)
    graph_attention_1 = GraphAttention(F_1,
                                        attn_heads=n_heads_1,
                                        attn_heads_reduction='concat',
                                        dropout_rate=dropout_rate,
                                        activation=LeakyReLU(alpha=0.2),
                                        kernel_initializer='he_uniform',
                                        bias_initializer='zeros',
                                        attn_kernel_initializer='he_uniform',
                                        kernel_regularizer=l2(l2_reg),
                                        attn_kernel_regularizer=l2(l2_reg))([dropout1, A_in])
    dropout2 = Dropout(dropout_rate)(graph_attention_1)
    graph_attention_2 = GraphAttention(F_3,
                                        attn_heads=1,
                                        attn_heads_reduction='average',
                                        kernel_initializer='random_uniform',
                                        bias_initializer='zeros',
                                        attn_kernel_initializer='random_uniform',
                                        dropout_rate=dropout_rate,
                                        activation='linear',
                                        kernel_regularizer=l2(l2_reg),
                                        attn_kernel_regularizer=l2(l2_reg))([dropout2, A_in])


    # Build model
    model = Model(inputs=[X_in, A_in], outputs=graph_attention_2)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer,
                    loss='mean_squared_error',
                    weighted_metrics=['acc'])

    # Load best model
    model.load_weights(getcwd() + './logs/best_model_l2.h5')

    # Evaluate model
    predictions = model.predict([X, A],
                                batch_size=N)
    visualize_output(predictions)
    serialize_output(predictions, mesh)


