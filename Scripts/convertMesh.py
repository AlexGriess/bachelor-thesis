#Author: Alexander Griessel
#Created On: 14/11/2019

import pywavefront
import numpy as np
from plyfile import PlyData, PlyElement
import os as file_manager
import json
from defs.mesh import Mesh
from pywavefront import visualization
import math
import vg
from random import randint
from random import random
from random import shuffle

def start():
    """Entry point"""
    meshes = []
    files = file_manager.listdir("./wavefronts")
    for file in files:
        name = file[:-4]
        if ".ply" in file:
            file = "./wavefronts/" + file
            print(file)
            mesh = Mesh(name)
            plydata = PlyData.read(file)
            vertexes = create_vertexes(mesh, ply_get_vertexes(plydata))
            faces = ply_get_faces(plydata)
            get_neighbors(mesh, vertexes, faces)
            get_vert_normals(mesh, plydata)
            convexities = get_convexity(mesh.vertices)
            #gen_vert_affordances(mesh, plydata)
            gen_ply_convexities(mesh, convexities, plydata, name)
            gen_center_offset(mesh, plydata, name)
            gen_convexity_deviation(mesh, plydata, name)
            mesh.get_positions()
            print(mesh.adjacency)
            meshes.append(mesh)
            save_grasping_pairs_2(mesh, name, plydata)
            #save_grasping_pairs(mesh, name, plydata)
    create_files(meshes)


def create_vertexes(mesh, positions):
    """Create all the vertex positions, set them in the mesh"""
    mesh.generate_vertices(positions)
    return positions


def get_convexity(vertexes):
    """Create and save all the face midpoints"""
    convexity = []
    for vert in vertexes:
        for neighbor in vert.neighbors:
            vert.neighbor_normals.append(vertexes[neighbor].normal)
            vert.neighbor_positions.append(vertexes[neighbor].position)
        convexity.append(vert.set_convexity())
    return convexity


def get_face_normals(mesh, vertexes, faces, normals=[]):
    """Calculate the face normals"""
    for face in faces:
        U = np.subtract(vertexes[face[1]], vertexes[face[0]])
        V = np.subtract(vertexes[face[2]], vertexes[face[0]])
        cross = np.cross(U, V)
        normal = np.divide(cross, np.linalg.norm(cross))
        normals.append(normal)
    face_norms = [[] for i in range(len(vertexes))]
    for i in range(len(vertexes)):
        for face in faces:
            if i in face:
                face_norms[i].append(normals[faces.index(face)])
    mesh.generate_normals(face_norms)
    return normals


def get_neighbors(mesh, vertexes, faces):
    """Find each vertexes neighbors"""
    neighbors = [[] for i in range(len(vertexes))]
    for i in range(len(vertexes)):
        mesh.adjacency[i][i] = 1
        for face in faces:
            if i in face:
                pos = face.index(i)
                if face[pos-2] not in neighbors[i]:
                    neighbors[i].append(face[pos-2])
                if face[pos-1] not in neighbors[i]:
                    neighbors[i].append(face[pos-1])
                mesh.adjacency[i][face[pos-2]] = 1
                mesh.adjacency[i][face[pos-1]] = 1
    mesh.set_neighbors(neighbors)
    return neighbors


def create_files(meshes):
    """Create the new .npy save formats"""
    for mesh in meshes:
        file_name = "./system/model/data/files/" + mesh.get_name()
        save_adjacency(mesh, file_name)
        save_features_grasps(mesh, file_name)
        save_features_prob(mesh, file_name)


def save_adjacency(mesh, file_name):
    """ Save the adjaceny matrix in a .dat"""
    np.save(file_name + "_mat.npy", mesh.get_adjacency())
    return mesh.get_adjacency()


def save_features_grasps(mesh, file_name):
    """Assorts features for the final grasping layer."""
    features = []
    for vert in mesh.vertices:
        row = [vert.position[0], vert.position[1], vert.position[2], vert.normal[0], vert.normal[1], vert.normal[2], vert.convexity]
        features.append(row)
    features = np.array(features)
    print(features.shape)
    np.save(file_name + "_aff_feats.npy", features)
    return features


def save_features_prob(mesh, file_name):
    """ Save vertexes and their features"""
    features = []
    for vert in mesh.vertices:
        row = [vert.surfaces[0], vert.surfaces[1], vert.surfaces[2], vert.surfaces[3], vert.surfaces[4], vert.con_dev, vert.convexity] #, vert.dist_center
        features.append(row)
    features = np.array(features)
    print(features.shape)
    np.save(file_name + "_feats.npy", features)
    with open("./system/model/data/data.json", "r") as data_header:
        data_json = json.load(data_header)
    with open("./system/model/data/data.json", "w+") as data_header:
        data_json[mesh.get_name()] = "." + file_name
        json.dump(data_json, data_header, sort_keys=True, indent=4, separators=(',', ': '))
    return features


def ply_get_faces(plydata):
    """ Get faces from Ply"""
    length = plydata['face'].count
    faces = []
    print(f"Length is: {length}")
    for i in range(length):
        faces.append(np.ndarray.tolist(plydata['face']['vertex_indices'][i]))
    return faces


def ply_get_vertexes(plydata):
    """ Get vertexes from Ply"""
    vertexes = []
    length = len(plydata['vertex']['x'])
    print(f"Length is: {length}")
    for i in range(length):
        x = float(plydata['vertex']['x'][i])
        y = float(plydata['vertex']['y'][i])
        z = float(plydata['vertex']['z'][i])
        vertexes.append((x, y, z))
    print(vertexes[0])
    return vertexes


def get_vert_normals(mesh, plydata):
    """ get all the vertex normals"""
    normals = []
    length = len(plydata['vertex']['x'])
    for i in range(length):
        nx = float(plydata['vertex']['nx'][i])
        ny = float(plydata['vertex']['ny'][i])
        nz = float(plydata['vertex']['nz'][i])
        if nx == 0 and ny == 0 and nz == 0:
            nx = 0.33
            ny = 0.33
            nz = 0.33
        normals.append([nx, ny, nz])
    mesh.generate_ply_normals(normals)
    return normals


def gen_vert_affordances(mesh, plydata):
    """ Save vertex affordances """
    length = len(plydata['vertex']['x'])
    affordances = plydata['vertex']['red']/255
    #mesh.vertices[i].affordance = a
    for i in range(len(affordances)):
        mesh.vertices[i].affordance = affordances[i]
    affordances = np.array(affordances)
    print(affordances.shape)
    np.save("./system/model/data/files/" + mesh.name + "_aff.npy", affordances)
    PlyData(plydata, text=True).write("./visualisations/" + mesh.name + "_aff.ply")
    return affordances


def gen_center_offset(mesh, plydata, name):
    "Save offset to center of mesh"
    x = np.array([vert.position[0] for vert in mesh.vertices])
    y = np.array([vert.position[1] for vert in mesh.vertices])
    z = np.array([vert.position[2] for vert in mesh.vertices])
    x_avg = (np.amax(x) + np.amin(x))/2
    y_avg = (np.amax(y) + np.amin(y))/2
    z_avg = (np.amax(z) + np.amin(z))/2
    major = 0
    dists = []
    mesh.center = [x_avg, y_avg, z_avg]
    for vert in mesh.vertices:
        vert.dist_center = vg.euclidean_distance(np.array(vert.position), np.array([x_avg, y_avg, z_avg]))
        if vert.dist_center > major: major = vert.dist_center
    for vert in mesh.vertices:
        vert.dist_center = vert.dist_center / major
        dists.append(255 - vert.dist_center * 255)
    plydata['vertex']['red'] = dists
    plydata['vertex']['green'] = dists
    plydata['vertex']['blue'] = dists
    PlyData(plydata, text=True).write("./visualisations/" + mesh.name + "_cent.ply")
    return dists


def gen_convexity_deviation(mesh, plydata, name):
    """ Saves the deviation of the vertex's convexity from the avg convexity"""
    convexities = np.array([abs(vert.convexity) for vert in mesh.vertices])
    conv_avg = np.mean(convexities)
    devs = []
    for (c, vert) in zip (convexities, mesh.vertices):
        con_dev = abs(abs(c) - conv_avg)
        vert.con_dev = con_dev
        devs.append(con_dev * 255)
    plydata['vertex']['red'] = devs
    plydata['vertex']['green'] = devs
    plydata['vertex']['blue'] = devs
    PlyData(plydata, text=True).write("./visualisations/" + mesh.name + "_cdev.ply")
    return devs


def gen_ply_convexities(mesh, convexity, plydata, name):
    """ Saves convexities as map on the ply verts"""
    red = []
    green = []
    blue = []
    y = []
    for (c, vert) in zip(convexity, mesh.vertices):
        if c < 0.015 and c > -0.015:
            red.append((int)(255 * -(abs(c*66.66) - 1)))
            green.append(0)
            blue.append(0)
            y.append([-(abs(c*66.66) - 1), 0, 0 ,0 ,0])
        elif c < 0.0:
            if c < -0.3:
                red.append((int)(-255*(c)))
                green.append((int)(-255*(c)))
                blue.append(0)
                y.append([0, -(3.333*c +1), 1, 0, 0])
            else:
                red.append(0)
                green.append((int)(-255*(2*c)))
                blue.append(0)
                y.append([0, 0, -(3.333*c), 0, 0])
                
        else:
            if c > 0.3:
                red.append((int)(255*(c)))
                green.append(0)
                blue.append((int)(255*(c)))
                y.append([0, 0, 0, (3.333*c -1), 1])
            else:
                red.append(0)
                green.append(0)
                blue.append((int)(255*(2*c)))
                y.append([0, 0, 0, 0, 3.333*c])
        vert.surfaces = y[-1]
    plydata['vertex']['red'] = red
    plydata['vertex']['green'] = green
    plydata['vertex']['blue'] = blue
    vertex = plydata['vertex']
    PlyData(plydata, text=True).write("./visualisations/" + name + "_surf.ply")
    s_con = [(c+1)*127 for c in convexity]
    plydata['vertex']['red'] = s_con
    plydata['vertex']['green'] = s_con
    plydata['vertex']['blue'] = s_con
    PlyData(plydata, text=True).write("./visualisations/" + name + "_con.ply")


def save_grasping_pairs_2(mesh, name, plydata):
    """Alternative classes"""
    index = np.load("./system/model/data/files/" + mesh.name + "_obj_index.npy")
    grasps = None
    with open("./system/model/data/files/" + name + "_adj.json", "r") as data_header:
        grasps = json.load(data_header)
        indexes = []
    positions = []
    for i in index:
        x = round(i[0], 8)
        y = round(i[1], 8)
        z = round(i[2], 8)
        positions.append([x,y,z])
    vertices = []
    for vert in mesh.vertices:
        x = round(vert.position[0], 8)
        y = round(vert.position[1], 8)
        z = round(vert.position[2], 8)
        vertices.append([x,y,z])
    for vert in vertices:
        try:
            indexes.append(positions.index(vert))
        except Exception:
            print(f"Failed to find vert {vert}")
            indexes.append(0)
    grasp_arr = []
    Y = []
    vert = 0
    for i in indexes:
        grasp_arr.append(grasps.get(f"{i}"))
    print(len(grasp_arr))
    need_false = False
    size = (len(indexes), len(indexes))
    matrix = np.zeros(size, dtype="int")
    print(f"Grasp matrix shape: {matrix.shape}")
    for g in range(len(grasp_arr)):
        grasp = grasp_arr[g]
        for i in range(len(grasp)):
            if grasp[i] != 0:
                for n in range(len(grasp_arr)):
                    if n != g:
                        vert = grasp_arr[n][i]
                        if vert != 0 and vert != grasp[i]:
                            matrix[g, n] += 1
                            matrix[n, g] += 1
    print(f"Grasp Matrix: {matrix}")
    Y = []
    largest = 0
    trues = 0
    for i in range(len(matrix)):
        for n in range(len(matrix[i])):
            if matrix[i][n] != 0:
                Y.append([i,n,matrix[i][n]])
                if random() > 0.5:
                    trues += 1
                if matrix[i][n] > largest:
                    largest = matrix[i][n]
    for x in range(trues):
        one = randint(0,len(indexes)-1)
        two = randint(0,len(indexes)-1)
        if matrix[one][two] == 0:
            Y.append([one, two, 0])
            Y.append([two, one, 0])
    for grasp in Y:
        grasp[2] = grasp[2] / largest
    shuffle(Y)
    Y = np.array(Y)
    print(f"Created Y, Y shape: {Y.shape}")
    print(f"Normalized Y: {Y}")
    np.save("./system/model/data/files/" + name + "_grasps.npy", Y)




def save_grasping_pairs(mesh, name, plydata):
    """Assembles the grasping pairs classes"""
    index = np.load("./system/model/data/files/" + mesh.name + "_obj_index.npy")
    grasps = None
    with open("./system/model/data/files/" + name + "_adj.json", "r") as data_header:
        grasps = json.load(data_header)
    indexes = []
    positions = []
    for i in index:
        x = round(i[0], 8)
        y = round(i[1], 8)
        z = round(i[2], 8)
        positions.append([x,y,z])
    vertices = []
    for vert in mesh.vertices:
        x = round(vert.position[0], 8)
        y = round(vert.position[1], 8)
        z = round(vert.position[2], 8)
        vertices.append([x,y,z])
    for vert in vertices:
        try:
            indexes.append(positions.index(vert))
        except Exception:
            print(f"Failed to find vert {vert}")
            indexes.append(0)
    
    grasp_arr = []
    Y = []
    vert = 0
    for i in indexes:
        grasp_arr.append(grasps.get(f"{i}"))
    print(len(grasp_arr))
    need_false = False
    for i in range(len(indexes)):
        grasp = grasp_arr[i]
        for g in range(len(grasp)):
            if grasp[g] != 0:
                n = 0
                for grsp in grasp_arr:
                    if grsp[g] != 0 and grsp[g] != grasp[g]:
                        if mesh.vertices[vert].affordance > 0.1 and mesh.vertices[n].affordance > 0.1:
                            p = (mesh.vertices[vert].affordance + mesh.vertices[n].affordance)/2
                            p = (mesh.vertices[vert].affordance * mesh.vertices[n].affordance)/p
                            Y.append([vert, n, p])
                            Y.append([n, vert, p])
                            need_false = True
                            break
                    n += 1
            elif need_false == True:
                n = randint(0, len(grasp_arr)-1)
                Y.append([vert, n, 0])
                Y.append([n, vert, 0])
                need_false = False
        vert += 1
    print(f"Length of Y: {len(Y)}")
    color_1 = [0 for i in range(len(indexes))]
    color_2 = [0 for i in range(len(indexes))]
    for y in Y:
        color_1[y[0]] += 10
        color_2[y[1]] += 10
    plydata['vertex']['red'] = color_1
    plydata['vertex']['green'] = color_2
    plydata['vertex']['blue'] = [0 for i in range(len(indexes))]
    PlyData(plydata, text=True).write("./visualisations/" + name + "_grasps.ply")
        
    Y = np.array(Y)
    print(Y.shape)
    np.save("./system/model/data/files/" + name + "_grasps.npy", Y)


                        

                




start()
