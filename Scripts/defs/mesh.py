#Class used to store new mesh features
from defs.vertex import Vertex
import numpy as np

class Mesh:
    def __init__(self, name):
        self.name = name
        self.vertices = []
        self.labels = []
        self.adjacency = None
        self.classes = None
        self.center = None

    def get_name(self):
        """Return the name of the object""" 
        return self.name

    def get_adjacency(self):
        """Return the adjacency matrix"""
        return self.adjacency

    def get_vertices(self):
        """Return all the vertices"""
        return self.vertices

    def get_normals(self):
        """Assemble and return all the vertex normals"""
        normals = []
        for vert in self.vertices:
            normals.append(vert.get_normal)
        return normals
    
    def get_positions(self):
        """Assemble and return all the vertex positions"""
        positions = []
        for vert in self.vertices:
            positions.append(vert.get_position())
        return positions

    def get_convexities(self):
        """Assemble and return all the vertex convexity values"""
        convexity = []
        for vert in self.vertices:
            convexity.append(vert.get_convexity)
        return convexity

    def generate_vertices(self, vertices):
        """Creates a new vertex object for each vertex in the mesh"""
        index = 0
        self.adjacency = np.zeros((len(vertices), len(vertices)))
        for vert in vertices:
            self.vertices.append(Vertex(index, vert))
            index += 1
    
    def generate_ply_normals(self, normals):
        length = len(self.vertices)
        for i in range(length):
            self.vertices[i].normal = normals[i]
        return self.vertices

    def generate_normals(self, face_norms):
        """Generate normals for each vertex"""
        for vert in self.vertices:
            vert.set_normal(face_norms[self.vertices.index(vert)])
            #print(f"Generated normals for vertex {face_norms[self.vertices.index(vert)]}")
        return self.vertices

    def generate_convexity(self, midpoints):
        """Generate each vertex's convexity value"""
        for i in range(len(self.vertices)):
            self.vertices[i].set_convexity(midpoints[i])
        return self.vertices

    def set_neighbors(self, neighbors):
        """Assign each vertexes neighbors"""
        for vertex in self.vertices:
            i = self.vertices.index(vertex)
            self.vertices[i].set_neighbors(neighbors[i])
            #print(f"Assigned neighbors to vertex {i}")
        return self.vertices