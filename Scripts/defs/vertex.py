#Definition of one vertex
import numpy as np
import vg
import math


class Vertex:
    def __init__(self, index, position):
        self.index = index
        self.neighbors = []
        self.position = position
        self.normal = (0,0,0)
        self.convexity = 0.0
        self.neighbor_normals = []
        self.neighbor_positions = []
        self.surfaces = []
        self.dist_center = 0.0
        self.affordance = 0.0
        self.con_dev = 0.0

    def get_index(self):
        """Return the index"""
        return self.index

    def get_neighbors(self):
        """Return the neigbors"""
        return self.neighbors

    def get_position (self):
        """Return the position"""
        return self.position

    def get_normal(self):
        """Return the normal"""
        return self.normal

    def get_convexity(self):
        """Return the convexity"""
        return self.convexity

    def set_neighbors(self, neighbors):
        """Set the neighbors"""
        self.neighbors = neighbors
    
    def set_normal(self, normals):
        """Calc and set the normal"""
        #Define relevant variables
        nSum = np.array([0.0, 0.0, 0.0])
        K = 0
        #Find the average normal of all the faces
        for n in normals:
            K = K + 1
            nSum = np.add(nSum, np.array(n))
        normal = np.divide(nSum, np.linalg.norm(nSum))
        #print(normal)
        self.normal = normal.tolist()
        return normal

    def set_convexity(self):
        """Calculate and save the convexity"""
        normals = self.neighbor_normals
        positions = self.neighbor_positions
        normal = np.array(self.normal)
        angles = []
        avg_angle = 0.0
        for i in range(len(normals)):
            neigh_norm = np.array(normals[i])
            dist = vg.euclidean_distance(np.array(positions[i]), np.array(self.position))
            new_pos_i = np.array(positions[i]) + 0.5 * dist * np.array(normals[i])
            new_pos_curr = np.array(self.position) + 0.5 * dist * np.array(self.normal)
            dist_alpha = vg.euclidean_distance(new_pos_i, new_pos_curr)
            angle = vg.angle(neigh_norm, normal)
            if dist_alpha < dist:
                angle = 0.0 - angle
            angles.append(angle)
            avg_angle += angle
        avg_angle = avg_angle / len(normals)
        convexity = avg_angle/90.0
        self.convexity = convexity
        return convexity



            
