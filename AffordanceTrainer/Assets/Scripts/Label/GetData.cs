﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetData : MonoBehaviour
{
    public GameObject[] trainingPrefabs;
    public int count = 0;
    public int total;
    // Start is called before the first frame update
    void Start()
    {
        trainingPrefabs = Resources.LoadAll<GameObject>("TrainingData");
        total = trainingPrefabs.Length;
    }
}
