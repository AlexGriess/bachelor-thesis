﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGrasp : MonoBehaviour
{
    public GameObject table;
    private Transform graspTrans;
    public bool startTest;
    public bool failed = false;
    private float timeMax = 4.0f;
    private float time = 0.0f;
    public int count = 0;
    WaitForSeconds waitForSeconds = new WaitForSeconds(0.01f);
    // Start is called before the first frame update
    void Start()
    {
        graspTrans = GameObject.Find("TestPos").transform;
        table = GameObject.Find("Table");
    }

    // Update is called once per frame
    void Update()
    {   
    }
    public void checkGrasp(GameObject Hand, GameObject HandParent){
        count = graspTrans.gameObject.GetComponent<GetData>().count;
        GameObject nextObject = graspTrans.gameObject.GetComponent<GetData>().trainingPrefabs[count];
        if(count +1 != graspTrans.gameObject.GetComponent<GetData>().total){
            graspTrans.gameObject.GetComponent<GetData>().count += 1;
        }
        else{
            graspTrans.gameObject.GetComponent<GetData>().count = 0;
        }
        GameObject[] objects = {Hand, HandParent, nextObject};

        StartCoroutine("startGrasp", objects);
    }

    IEnumerator startGrasp(GameObject[] objects) {
        GameObject nextObj = objects[2];
        failed = false;
        while(true){
            if(failed == true){
                objects[2] = gameObject;
            }
            else{
                objects[2] = nextObj;
            }
            time += 0.01f;
            if(time > timeMax){
                break;
            }
            yield return waitForSeconds;
        }
        if(failed == false){
            yield return gameObject.GetComponent<vertDetect>().recalcCollision = true;
        }
        ReStartSim(objects[0], objects[1], objects[2]);
    }

    void ReStartSim(GameObject Hand, GameObject HandParent, GameObject newObject){
        Hand.transform.position = HandParent.transform.position;
        Hand.transform.rotation = HandParent.transform.rotation;
        Hand.transform.parent = HandParent.transform;
        GameObject newObj = Instantiate (newObject) as GameObject;
        newObj.transform.position = graspTrans.transform.position;
        newObj.transform.rotation = Random.rotation;
        time = 0.0f;
        Destroy(gameObject);

    }
    void OnCollisionEnter(Collision other){
        GameObject collider = other.gameObject;
        if(collider == table){
            failed = true;
        }
    }
}
