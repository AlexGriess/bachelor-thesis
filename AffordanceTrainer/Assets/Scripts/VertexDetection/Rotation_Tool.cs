﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(FixedJoint))]
[RequireComponent(typeof(Rigidbody))]
public class Rotation_Tool : MonoBehaviour {

	private bool isCurrentlyInBody = false; 
	private Rigidbody rb;
	// The last position when the Pivot was set, to set a new position.
	private Vector3 lastParentPosition;
	// Our last position when the Pivot was set, to calculate the difference.
	private Vector3 lastOwnPosition;
	private Vector3 currentRotation = new Vector3(60.0f, 60.0f, 60.0f);
	private Vector3 newRotation = new Vector3(0.0f, 0.0f, 0.0f);
	private const float minRotationAngle = -40.0f;
	private const float maxRotationAngle = 40.0f;
	private const float rotationSpeed = 2.0f;
	// Use this for initialization
	private void Start () {
		rb = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	private void Update () {
		if (isCurrentlyInBody) {
			// Boundaries
			currentRotation.x = Mathf.Clamp(currentRotation.x, minRotationAngle, maxRotationAngle);
			currentRotation.y = Mathf.Clamp(currentRotation.y, minRotationAngle, maxRotationAngle);
			currentRotation.z = Mathf.Clamp(currentRotation.z, minRotationAngle, maxRotationAngle);

			newRotation.x = Input.GetAxis("Horizontal") * currentRotation.x;
			//newRotation.y = Input.GetAxis("TO BE FILLED") * currentRotation.x;
			newRotation.z = Input.GetAxis("Vertical") * currentRotation.z;
			SetRotation(newRotation);

			if (Input.GetKeyDown(KeyCode.L))
				Snap();
		}
	}

	// Set the parent position for a new Pivot
	private void Snap() {
		if (isCurrentlyInBody) {
			transform.parent.position = lastParentPosition = lastParentPosition + transform.position - lastOwnPosition;
			transform.position -=
			lastOwnPosition = transform.position;
			Debug.Log(lastOwnPosition + " " + lastParentPosition);
		}
	}

	// Call this after the Tool left the body
	private void Reset() {
		isCurrentlyInBody = false;
		rb.constraints = RigidbodyConstraints.None;
		gameObject.GetComponent<BoxCollider>().isTrigger = false;
	}

	// Is called whenever the Tool collides with an Object
	private void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Body"){
			isCurrentlyInBody = true;
			// disable movement after the first collision
			rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
			// set Tool as a trigger to avoid further collisions
			gameObject.GetComponent<BoxCollider>().isTrigger = true;

			transform.parent.position = lastParentPosition = col.contacts[0].point;
			lastOwnPosition = transform.position;
		}
	}

	private void SetRotation(Vector3 rotation) {
		Quaternion target = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
		transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, target, Time.deltaTime * rotationSpeed);
	}
}
