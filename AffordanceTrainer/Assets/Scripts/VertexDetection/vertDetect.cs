﻿/* Author: Alexander Griessel
    Note: Many instances of legacy code
 */
using Newtonsoft.Json;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityExtension;
using static System.Environment;

public class vertDetect : MonoBehaviour
{
    public Mesh detector;
    public Vector3[] detVerts;
    public Color[] detState;
    public float avgDist = 0.00f;
    public vertActor[] actors;
    public Collider[] act_collider;
    public float hit = 0.0f;
    public float collSens;
    public bool recalcCollision;
    private bool getActors = true;
    public bool near = false;
    public int[] coll;
    WaitForSeconds waitForSeconds = new WaitForSeconds(0.01f);

    // Start is called before the first frame update
    void Start()
    {
        actors = FindObjectsOfType<vertActor>();
        recalcCollision = false;
        detector = GetComponent<MeshFilter>().mesh;     //Assign the connected mesh as the detector
        detVerts = detector.vertices;                   //Get the detector vertices
        detState = new Color[detVerts.Length];          //Color array to denote collisions
        //avgDist = 3 * Vector3.Distance(transform.TransformPoint(detVerts[1]), transform.TransformPoint(detVerts[0])); //Get a normalized distance. This would be the size of the gripper.
        StartCoroutine("CalcAvgDist");
    }
    
    void Update(){
        if(recalcCollision){
            findAllActors();
            UpdateVerts();                  //Launch vertex detection
            CalculateCollision();
            StartCoroutine("SerializeData");
            StartCoroutine("ExportObj");
            recalcCollision = false;
        }
        if(getActors){
            getActors = false;
            findAllActors();
        }
    }
    void OnCollisionEnter(Collision other){
        GameObject Object = other.gameObject;
        if(Object.name == "Pincer"){
            near = true;
        }
    }
    void OnCollisionExit(Collision other){
        GameObject Object = other.gameObject;
        if(Object.name == "Pincer"){
            near = false;
        }
    }

    //Check for vertex collision
    void UpdateVerts() {
        coll = new int[detVerts.Length];
        if(actors.Length == 0){
            findAllActors();
        }
        for(int i = 0; i < detVerts.Length; i++){
            for(int x = 0; x < actors.Length; x++){                                                 //Check every vertice for a collision
                bool inside = IsInside(actors[x].GetComponent<Collider>(), transform.TransformPoint(detVerts[i]));                                             
                if(inside == true){                                                                     //If a vertice is close enough to the actor
                    detState[i] = Color.green;
                    coll[i] = x + 1;
                    break;
                }
                else{
                    coll[i] = 0;
                }
            }
        }
        detector.colors = detState; //Update 'heat' for ever vertice
    }

    //This function 'cleans' the collision data, to only include 'strong' collisions
    void CalculateCollision(){
        for(int i = 0; i < detVerts.Length; i++){
            if(detState[i][1] > collSens){
                detState[i] = Color.white;
            }
            else{
                detState[i] = Color.black;
            }
        }
        detector.colors = detState;
    }

    IEnumerator CalcAvgDist(){
        for(int i = 0; i < detVerts.Length; i++){
            avgDist += Vector3.Distance(detVerts[i], new Vector3(0.0f, 0.0f, 0.0f));
        }
        avgDist = avgDist / detVerts.Length;
        avgDist = avgDist / Mathf.Pow(detVerts.Length, 1.0f / 6.0f);
        yield return null;
    }
    //Save the collision data
    IEnumerator SerializeData(){
        string workingDir = CurrentDirectory + "/../Scripts/system/model/data/files/";
        string path = workingDir + gameObject.name.Split('(')[0] + "_unity_lbl.json";
        string path_2 = workingDir + gameObject.name.Split('(')[0] + "_adj.json";
        Debug.Log("Path: " + path);
        OrgData objData;
        string json = "";
        if(File.Exists(path)){
            using (StreamReader r = new StreamReader(path)){
                json = r.ReadToEnd();
                objData = JsonUtility.FromJson<OrgData>(json);
                Debug.Log(objData);
                Debug.Log(objData.Grasped);
                if(objData.Grasped.Length != detVerts.Length){
                    objData.Grasped = new int[detVerts.Length];
                    for(int i = 0; i < detVerts.Length; i++){
                        objData.Grasped[i] = 0;
                    }
                }
            }
        }
        else{
            objData = new OrgData();
            objData.Grasped = new int[detVerts.Length];
            for(int i = 0; i < detVerts.Length; i++){
                objData.Grasped[i] = 0;
            }
        }
        for(int i = 0; i < detVerts.Length; i++){
            if(detState[i] == Color.white){
                objData.Grasped[i] = objData.Grasped[i] + 1;
            }
        }
        json = JsonUtility.ToJson(objData, true);
        Debug.Log(json);
        StreamWriter sw = File.CreateText(path);
        sw.Close();
        File.WriteAllText(path, json);
        json = "";

        Adjacency adj = new Adjacency();
        if(File.Exists(path_2)){
            Debug.Log("File Exists");
            using (StreamReader r =  File.OpenText(path_2)){
                //json = r.ReadToEnd();
                JsonSerializer serializer = new JsonSerializer();
                adj.grasps = (Dictionary<int, int[]>)serializer.Deserialize(r, typeof(Dictionary<int, int[]>));
                Debug.Log(adj.grasps);
                if(adj.grasps.Count == detVerts.Length){
                    Debug.Log("File Correct");
                    for(int i = 0; i < detVerts.Length; i++){
                        int len = adj.grasps[i].Length;
                        int[] grasps = adj.grasps[i];
                        Array.Resize<int>(ref grasps, len + 1);
                        grasps[len] = coll[i];
                        adj.grasps[i] = grasps;
                    }
                }
                else{
                    Debug.Log("File InCorrect");
                    Debug.Log(detVerts.Length);
                    Debug.Log(adj.grasps.Count);
                    adj.grasps = new Dictionary<int, int[]>();
                    for(int i = 0; i < detVerts.Length; i++){
                        int[] grasp = {coll[i]};
                        adj.grasps.Add(i, grasp);
                    }
                }
            }
        }
        else{
            Debug.Log("File Does not Exist");
            adj.grasps = new Dictionary<int, int[]>();
            for(int i = 0; i < detVerts.Length; i++){
                int[] grasp = {coll[i]};
                adj.grasps.Add(i, grasp);
            }
        }
        Debug.Log(adj.grasps[2].Length);
        json = JsonConvert.SerializeObject(adj.grasps);
        Debug.Log(json);
        sw = File.CreateText(path_2);
        sw.Close();
        File.WriteAllText(path_2, json);
        Debug.Log("Completed");
        yield return null;
    }
    
    IEnumerator ExportObj(){
        string workingDir = CurrentDirectory + "/../Scripts/system/model/wavefronts/";
        string path = workingDir + gameObject.name.Split('(')[0] + ".obj";
        if(File.Exists(path)){
            File.Delete(path);
        }
        FileStream fs = new FileStream(path, FileMode.Create);
        var lObjData = GetComponent<MeshFilter>().mesh.EncodeOBJ();
        OBJLoader.ExportOBJ(lObjData, fs);
        fs.Close();
        yield return null;
    }

    public void findAllActors(){
        actors = FindObjectsOfType<vertActor>();
    }

    public static bool IsInside(Collider c, Vector3 point)
    {
        bool contains = c.bounds.Contains(point);
        return contains;
    }
}



public class OrgData
{
    public int[] Grasped;
}

public class Adjacency
{
    public Dictionary<int, int[]> grasps;
}

public class Adj_list
{
    public int[] adj_list;
}