﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadCollision : MonoBehaviour
{
    WaitForSeconds waitForSeconds = new WaitForSeconds(0.01f);
    public GameObject Object;
    public Transform pos;
    public Vector3[] verts;
    public int length;
    public bool colliding = false;
    public bool near = false;
    public bool started = false;
    int vertnum = 0;
    Mesh mesh;
    public float dist;

    void Start(){
    }

    void FixedUpdate(){
            if(near){
                started = true;
                for(int i = 0; i < length; i++){
                    vertnum = i;
                    dist = Vector3.Distance(pos.position, transform.TransformPoint(verts[i]));
                    if(dist < 7.0f){
                        colliding = true;
                        break;
                    }
                    colliding = false;
                }
            }
            else{
                colliding = false;
            }
    }
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.GetComponent<vertDetect>() != null){
            near = true;
            Object = other.gameObject;
            mesh = Object.GetComponent<MeshFilter>().mesh;
            verts = mesh.vertices;
            length = verts.Length;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if(other.gameObject.GetComponent<vertDetect>() != null){
            near = false;
        }
    }
}
