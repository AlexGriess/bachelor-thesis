﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NewtonVR;

public class PincerClose : MonoBehaviour
{
    public GameObject RightHand;
    public GameObject LeftHand;
    public Transform LeftPincer;
    public Transform RightPincer;
    public Transform TestGrasp;
    public GameObject inter_obj;
    GameObject LPincer;
    GameObject RPincer;
    public float maxMove = 0.15f;
    public float currentMoveRight = 0.0f;
    public float currentMoveLeft = 0.0f;
    public bool test = false;
    public bool left = true;
    public bool right = true;
    // Start is called before the first frame update
    void Start()
    {
        TestGrasp = GameObject.Find("TestPos").transform;
        RightHand = GameObject.Find("RightHand");
        LeftHand = GameObject.Find("LeftHand");
        LPincer = LeftPincer.transform.Find("Pincer").gameObject;
        RPincer = RightPincer.transform.Find("Pincer").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        GetPadStatus();
        if(CheckUpdatePincerRight())
        {
            ClosePincer();
        }
        else if (CheckUpdatePincerLeft())
        {
            OpenPincer();
        }
        if(left == false && right == false){
            if(RightHand.GetComponent<NVRHand>().HoldButtonPressed){
                inter_obj = LPincer.GetComponent<PadCollision>().Object;
                inter_obj.GetComponent<Rigidbody>().isKinematic = true;
                inter_obj.transform.parent = gameObject.transform;
                Transform handParent = gameObject.transform.parent;
                gameObject.transform.SetParent(null);
                gameObject.transform.position = TestGrasp.transform.position;
                gameObject.transform.rotation = TestGrasp.transform.rotation;
                inter_obj.transform.SetParent(null);
                inter_obj.GetComponent<Rigidbody>().isKinematic = false;
                inter_obj.GetComponent<TestGrasp>().checkGrasp(gameObject, handParent.gameObject);
                inter_obj = LPincer.GetComponent<PadCollision>().Object;
                inter_obj.GetComponent<Rigidbody>().isKinematic = true;
                inter_obj.transform.parent = gameObject.transform;
            }
            if(LeftHand.GetComponent<NVRHand>().HoldButtonPressed){
                inter_obj = LPincer.GetComponent<PadCollision>().Object;
                inter_obj.GetComponent<Rigidbody>().isKinematic = true;
                inter_obj.transform.parent = gameObject.transform;
                Transform handParent = gameObject.transform.parent;
                gameObject.transform.SetParent(null);
                gameObject.transform.position = TestGrasp.transform.position;
                gameObject.transform.rotation = TestGrasp.transform.rotation;
                inter_obj.transform.SetParent(null);
                inter_obj.GetComponent<Rigidbody>().isKinematic = false;
                inter_obj.GetComponent<TestGrasp>().checkGrasp(gameObject, handParent.gameObject);
                inter_obj = LPincer.GetComponent<PadCollision>().Object;
                inter_obj.GetComponent<Rigidbody>().isKinematic = true;
                inter_obj.transform.parent = gameObject.transform;
            }
        }
    }
    

    void FixedUpdate(){
    }

    bool CheckUpdatePincerRight()
    {
        if(RightHand.GetComponent<NVRHand>().UseButtonPressed){
            return true;
        }
        else{
            return false;
        }
    }

    bool CheckUpdatePincerLeft()
    {
        if(LeftHand.GetComponent<NVRHand>().UseButtonPressed){
            return true;
        }
        else{
            return false;
        }
    }
    
    void ClosePincer()
    {
        if(currentMoveLeft <= maxMove){
            LeftPincer.Translate(new Vector3(0,0.0003f,0),Space.Self);
            currentMoveLeft += 0.0003f;
        }
        if(currentMoveRight <= maxMove){
            RightPincer.Translate(new Vector3(0,-0.0003f,0),Space.Self);
            currentMoveRight += 0.0003f;
        }
    }

    void OpenPincer()
    {
        if(currentMoveLeft >= 0.0f){
            LeftPincer.Translate(new Vector3(0,-0.0003f,0),Space.Self);
            currentMoveLeft -= 0.0003f;
        }
        if(currentMoveRight >= 0.0f){
            RightPincer.Translate(new Vector3(0,0.0003f,0),Space.Self);
            currentMoveRight -= 0.0003f;
        }
    }

    void GetPadStatus(){
        if(LPincer.GetComponent<PadCollision>().colliding == true){
            left = false;
        }
        else{
            left = true;
        }
        if(RPincer.GetComponent<PadCollision>().colliding == true){
            right = false;
        }
        else{
            right = true;
        }
    }
}
