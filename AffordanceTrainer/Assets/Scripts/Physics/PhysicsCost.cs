﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Set a cost function for grasps
public class PhysicsCost : MonoBehaviour
{
    public float effectorAngle;                         //Angle of effector to ground
    public float mu;                                    //Coeffiecnt of friction
    float gravity = 9.8f;                               //Acceleration due to gravity
    Vector3 accell = new Vector3(0.0f, 0.0f, 0.0f);     //Acceleration due to movement
    public float fMax = 1.0f;                           //Maximum force to be applied by the pincher
    public float fMin = 1.0f;                           //Minimum force to be applied by the pincher

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Friction is mu times the normal force, mass * a angle alpha
    public float getFrictionForce(float eA, float u, float fN){
        float frictionForce = u*fN;
        return frictionForce;
    }

    //Normal force is the force of gravity opposing some slope
    public float getNormalForce(float eA, float g, float a, float m){
        float normalForce = Mathf.Sin(eA)*m*(g+a);
        return normalForce;
    }

    //Normal force opposing friction from grasper
    public float getOpposingForce(float eA, float g, float a, float m, float u){
        float normalForce = getNormalForce(eA, g, a, m);
        float opposingForce = normalForce - getFrictionForce(eA, u, normalForce);
        return opposingForce;
    }

    //Determine slip vector, if force is not enough
    public Vector3 getSlipVector(float oF, float pF){
            return new Vector3(0.0f, 0.0f, 0.0f);
    }
}
