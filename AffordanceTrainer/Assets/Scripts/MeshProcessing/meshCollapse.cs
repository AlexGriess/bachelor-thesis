﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshCollapse : MonoBehaviour
{
    Mesh mesh;
    Vector3[] verts;
    int[] triangles;
    bool[] vertCent;
    Color[] colors;
    WaitForSeconds waitForSeconds = new WaitForSeconds(0.01f);
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        verts = mesh.vertices;
        triangles = mesh.triangles;
        colors = new Color[verts.Length];
        vertCent = new bool[verts.Length];
        StartCoroutine("UpdateVerts");
    }

    IEnumerator UpdateVerts(){
        Vector3[] newVerts = new Vector3[triangles.Length/3];
        int x = 0;
        for(int i = 0; i < triangles.Length; i+=3){
                newVerts[i/3] = (verts[triangles[i]] + verts[triangles[i+1]] + verts[triangles[i+2]])/3;
        }
        int[] newTris = new int[newVerts.Length];
        for(int i = 0; i < (newVerts.Length - newVerts.Length%3); i+=1){
            newTris[i] = i;
        }

        mesh.vertices = newVerts;
        //mesh.triangles = newTris;
        yield return waitForSeconds;
    }
}
