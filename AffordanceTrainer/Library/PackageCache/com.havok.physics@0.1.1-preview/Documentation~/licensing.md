
## Licensing Model

The Havok.Physics package is free to download from the Unity package manager. However, using the features at runtime may require a license, as described below.

### Before January 15th 2020 (Trial Period)

The current version of this package is a trial version that runs until January 15th 2020.

During the trial period the runtime features are free for everyone to use.

After the trial period, the runtime features will no longer function in the editor. In order to continue using Havok Physics you will have to upgrade to a newer version when that becomes available.

### After January 15th 2020

After the trial period, depending on your Unity plan a Havok Physics subscription _may_ be required to use the runtime features in the Unity Editor:

* Unity Personal : Free to use
* Unity Plus : Free to use
* Unity Pro : Havok Physics subscription required, available from the Unity Asset Store for $20 per seat per month
 
When using this package, the Unity Editor will perodically check whether you are entitled to use the runtime features.
The runtime features will be locked if you are not signed into Unity, or you are a Unity Pro user without a valid subscription.
Once unlocked, there is an allowance for working offline as the same user for up to one month before the subscription must be reconfirmed.

Note that the runtime features are always unlocked for standalone applications that have been built from Unity - standalone applications do not require an internet connection for Havok Physics to function.

Havok SDK licensees are entitled to use the runtime without any additional subscriptions - please [contact Havok](mailto:havosali@microsoft.com) for details.
