
## Supported Platforms

Havok.Physics for Unity is currently supported on the following platforms:

* Windows
* Mac
* Linux
* iOS
* Android

Support for the following platforms is coming soon. Note that these will require that you download additonal files from the platform holder websites, since they cannot be included in the package itself due to NDA restrictions.

* Xbox One
* Playstation 4
* Nintendo Switch
