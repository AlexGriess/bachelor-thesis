# Changelog
All notable changes to this project will be documented in this file.

## [0.1.1-preview] - 2019-09-20

- First public release

### Changed
- The plugins are now free to use for everyone until January 15th 2020

### Fixed
- Fixed potential IndexOutOfRangeException when executing IBodyPairsJob, IContactsJob or IJacobiansJob

## [0.1.0-preview.2] - 2019-09-05

### Fixed
- Bodies tagged for contact welding now actually get welding applied

## [0.1.0-preview.1] - 2019-08-29

- First pre-release package version
